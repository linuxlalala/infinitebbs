C=gcc
CFLAGS=-std=c99 -Wall -Wunused -Werror -Wextra -pedantic -lpq -lm -I$(shell pg_config --includedir)

all: bbs
bbs: bbs.o sql_func.o
	$(CC) $(CFLAGS) bbs.o sql_func.o -o bbs
debug: bbs.o sql_func.o
	$(CC) $(CFLAGS) -fsanitize --trapv -g bbs.o sql_func.o -o bbs
clean:
	rm -rf bbs *.o
