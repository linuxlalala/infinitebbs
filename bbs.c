/*InfiniteBBS, writen by infiniteNOP (John Tzonevrakis), licensed under the MIT license. :
Copyright (c) 2015, John Tzonevrakis

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
/*Just so gcc doesn't moan about "implicit declarations"...*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include "sql_func.h"
void signalhandler(int signal);
void show_help(void);
int main(int argc, char* argv[]) {
    signal(SIGINT, signalhandler);
    signal(SIGQUIT, signalhandler);
    char msgtitle[64];
    char usermessage[1024];
    char username[64];
    char passwd[4096];
    size_t len = 0;
    int msgid = 0;
    if(argc < 2) {
        fprintf(stderr, "Error: Not enough arguments\n");
        show_help();
        exit(EXIT_FAILURE);
    }
    if(strcmp(argv[1], "about") != 0 && strcmp(argv[1], "post") != 0 && strcmp(argv[1], "read") != 0 && strcmp(argv[1], "list") != 0) {
         fprintf(stderr, "Error: unknown command\n");
         show_help();
         exit(EXIT_FAILURE);
     }
    /*If the user only wants to read the credits, then we don't need to authenticate him/her:*/
    if(strcmp(argv[1], "about") == 0) {
        printf("InfiniteBBS: Written by infiniteNOP. Intended as an anapnea community project. Licensed under the MIT License. Comes with ABSOLUTELY NO WARRANTY.\n");
        exit(EXIT_SUCCESS);
    }
    /*TODO: Turn echo off.*/
    printf("Username: (max. 64 characters)\n");
    fgets(username, (int)sizeof(username), stdin);
    printf("Password: (max. 4096 characters)\n");
    fgets(passwd, (int)sizeof(passwd), stdin);
    printf("Attempting to open database...\n"); 
    open_db("bbs", username, passwd, "127.0.0.1", 5432);
    /*Blank out username and password.*/
    memset(username, '0', sizeof(username));
    memset(passwd, '0', sizeof(passwd));
    if(strcmp(argv[1], "list") == 0) {
        get_messages();
    } 
    else if(strcmp(argv[1], "post") == 0) {
        printf("Enter title:\n");
        fgets(msgtitle, (int)sizeof(msgtitle), stdin);
        printf("Enter message:\n"); 
        fgets(usermessage, (int)sizeof(usermessage), stdin);
        /*Eat last newline from title and message*/
        len = strlen(msgtitle); 
        if( msgtitle[len-1] == '\n' ) msgtitle[len-1] = '\0';
        len = strlen(usermessage); 
        if( usermessage[len-1] == '\n' ) usermessage[len-1] = '\0';
        if(argv[2] != '\0') {
            write_reply(msgtitle, usermessage,argv[2]);
        }
        else {
            write_message(msgtitle, usermessage);
        }
    }
    else if(strcmp(argv[1], "read") == 0) {
        if(argc != 3) {
            fprintf(stderr, "Error: Not enough arguments.");
            exit(EXIT_FAILURE);
        } 
        else {
            msgid = atoi(argv[2]);
            get_message(msgid);
        }
    }
    /*We're finished, close the db and return success.*/
    close_db();
    return 0;
}

void signalhandler(int signal) {
    printf("Interrupted. by %d\n", signal);
    close_db();
    exit(EXIT_FAILURE);
}

void show_help(void) {
         printf("Usage: bbs command [postid]\n");
         printf("List of commands:\n");
         printf("about -- Show credits\n");
         printf("post [postid] -- Posts a message. If supplied with postid, post a reply to the post whose id equals postid.\n");
         printf("read postid -- Reads a message.\n");
         printf("list -- Lists today's messages.\n");
}
