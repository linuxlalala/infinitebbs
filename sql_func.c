/*This module is part of InfiniteBBS. Licensed under the MIT license. See COPYING for details.
This module contains the database manipulation functions*/
#define _GNU_SOURCE
#include <libpq-fe.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
PGconn *bbs;

int open_db(char database[], char username[], char passwd[], char ipaddr[], int port) {
    char *conninfo;
    /*Synthesize conninfo*/
    if (asprintf(&conninfo, "dbname=%s user=%s password=%s hostaddr=%s port=%d", database, username, passwd, ipaddr, port) != -1) {
        /*Connect to the database server*/
        bbs = PQconnectdb(conninfo);
        free(conninfo);
    }
    /*Blank out the credentials*/
    memset(username, '0', strlen(username));
    memset(passwd, '0', strlen(passwd)); 
    if (PQstatus(bbs) != CONNECTION_OK) {
        fprintf(stderr, "Connection to the BBS database failed. Please report this error. The error was: %s", PQerrorMessage(bbs));
        PQfinish(bbs);
        exit(EXIT_FAILURE);
    }
    else {
        return(0);
    }
}

void close_db() {
    /*Close the database*/
    PQfinish(bbs);
}

/*Database query functions*/
/*Pull today's messages*/
int get_messages() {
    PGresult *res;
    const char *sql = "SELECT id,title,author FROM messages WHERE date = current_date AND replyto IS NULL;";
    int num_records;
    int i;
    /*Run the SQL query*/
    res = PQexec(bbs, sql);
    if (PQresultStatus(res) != PGRES_TUPLES_OK) {
        fprintf(stderr, "Pulling messages failed. Please report this error. The SQL error message was: %s\n", PQerrorMessage(bbs));
        PQclear(res);
        PQfinish(bbs);
        exit(EXIT_FAILURE);
    }
    else {
        num_records = PQntuples(res);
        printf("ID | title | author \n");
        for (i = 0; i < num_records; i++) {
            printf(" %s | %s | %s\n", PQgetvalue(res, i, 0), PQgetvalue(res, i, 1), PQgetvalue(res, i, 2)); 
        }
    }	
        PQclear(res);
        return 0;
}

int write_message(char* title, char* message) {
    PGresult *res;
    /*Create the SQL query.*/
    const char *values[2] = {title, message};
    int lengths[2] = {strlen(title), strlen(message)};
    int binary[2] = {0, 0};
    const char* sql = "insert into messages (title, message, date) values ($1::varchar, $2::varchar, current_date);";
    res = PQexecParams(bbs,sql, 2,NULL,values,lengths,binary,0);
    if (PQresultStatus(res) != PGRES_COMMAND_OK) {
        fprintf(stderr, "Posting message failed. Please report this error. The SQL error message was: %s\n", PQerrorMessage(bbs));
        PQclear(res);
        PQfinish(bbs);
        exit(EXIT_FAILURE);
    }
    else {
	printf("Successfully posted message");
        PQclear(res);
        return 0;
    }
}
int get_message(int id) {
    PGresult *res = NULL;
    int num_records;
    int i;
    char *sql = NULL;
    if(asprintf(&sql, "SELECT title,author,message FROM messages where id = %d OR replyto = '%d';", id, id) != -1)
    {
        res = PQexec(bbs, sql);
        free(sql);
    }
    if (PQresultStatus(res) != PGRES_TUPLES_OK) {
        fprintf(stderr, "Reading message failed. Please report this error. The SQL error message was: %s\n", PQerrorMessage(bbs));
        PQclear(res);
        PQfinish(bbs);
        exit(EXIT_FAILURE);
    }
    else {
        num_records = PQntuples(res);
        for (i = 0; i < num_records; i++) {
            printf("%s by %s\n%s\n", PQgetvalue(res, i, 0), PQgetvalue(res,i,1), PQgetvalue(res,i,2));
        }
    }
    PQclear(res);
    return 0;
}
int write_reply(char* title, char* message,char* id) {
    PGresult *res;
    /*Create the SQL query.*/
    const char *values[3] = {title, message, id};
    int lengths[3] = {strlen(title), strlen(message), strlen(id)};
    int binary[3] = {0,0,0};
    const char* sql = "insert into messages (title, message, date, replyto) values ($1::varchar, $2::varchar, current_date, $3::varchar);";
    res = PQexecParams(bbs,sql, 3,NULL,values,lengths,binary,0);
    if (PQresultStatus(res) != PGRES_COMMAND_OK) {
        fprintf(stderr, "Posting message failed. Please report this error. The SQL error message was: %s\n", PQerrorMessage(bbs));
        PQclear(res);
        PQfinish(bbs);
        exit(EXIT_FAILURE);
    }
    else {
        printf("Successfully posted message");
        PQclear(res);
        return 0;
    }
}

