<head>
<title>InfiniteBBS Web Front-End</title>
</head>
<body>
<?php
/*InfiniteBBS PHP read-only front-end, written by infiniteNOP. Requires PHP 5 >=5.4.0. Licensed under the MIT license. See COPYING for details.*/
$bbs = connect_db();
/*Do we need to show a specific post?*/
if(htmlspecialchars($_GET["post"])) { 
	/*Check whether the argument is numeric or not.*/
        if(is_numeric($_GET["post"]) == TRUE){
		get_message($bbs, $_GET["post"]);
        } else {
		/*If not, return error 400.*/
		http_response_code(400);
		printf("<b>Error 400 Bad request.</b>");
		exit();
	}
}
else {
	/*If not, list all of today's messages.*/
	list_messages($bbs);
}
function connect_db()
{
/*Connect to the database*/
	$db = pg_connect( "host=127.0.0.1 port=5432 dbname=bbs user=guest password=guest" );
	if(!$db){
		/*If connection was impossible, return 500 Internal Server Error.*/
		http_response_code(500);
		printf("<b>500 Internal Server error:</b>\n<em>Unable to connect to the database. Please report this.</em>\n");
		exit;
	}
	return $db;
}
function list_messages($db)
{
	$sql = "SELECT title,author,id FROM messages WHERE date = current_date;";
	$ret = pg_query($db, $sql);
	if(!$ret) {
		http_response_code(500);
		printf("<b>500 Internal Server error</b>\n<em>FATAL: Unable to execute query. Please report this.</em>\n");
		exit;
	}
	printf("<h1>Today's Messages</h1>");
	while($row = pg_fetch_row($ret)) {
		printf("<p><a href=index.php?post=%s>%s by %s</a></p>" ,htmlspecialchars($row[2]), htmlspecialchars($row[0]), htmlspecialchars($row[1])); 
	}
}
function get_message($db, $id) 
{
	$sql = "SELECT title,author,message FROM messages where id = $1;";
	$ret = pg_query_params($db, $sql, array($id));
	if(!$ret) {
		http_response_code(500);
		printf("<b>500 Internal Server Error.</b>\n<em>FATAL: Unable to execute query. Please report this.</em>\n");
		exit;
	}
	while($row = pg_fetch_row($ret)) {
		printf("<em>%s by %s</em><p>%s</p>", htmlspecialchars($row[0]), htmlspecialchars($row[1]), htmlspecialchars($row[2]));
	}
}
?>
<p><em>Written by John Tzonevrakis.</em></p>
<a href="https://bitbucket.org/linuxlalala/infinitebbs"><em>We're Opensource!</a></a>
</body>
