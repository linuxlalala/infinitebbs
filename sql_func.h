int open_db(char database[], char username[], char passwd[], char ipaddr[], int port);
int get_messages();
void close_db();
int write_message(char* title, char* message);
int get_message(int id);
int write_reply(char* title, char* message,char* id);
